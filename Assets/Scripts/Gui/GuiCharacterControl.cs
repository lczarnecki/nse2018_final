﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(AICharacterControl))]
[RequireComponent(typeof(SpeechAnimScript))]
[RequireComponent(typeof(EyesAnimScript))]
public class GuiCharacterControl : MonoBehaviour {

	private int toolBarMoveId = 1;
	private string[] textToolBarMove = { "Gauche", "Centre", "Droite" };

	private int toolBarExpressionId = 0;
	private string[] textToolBarExpression = { "Attente", "Sourire", "Triste", "Sans expression" };

	private bool avatarSpeech = false;


	private AICharacterControl aICharacter;
	private SpeechAnimScript speechAnim;
	private EyesAnimScript eyesAnim;

	public GameObject TargetRight;
	public GameObject TargetLeft;
	public GameObject TargetCenter;


	// Use this for initialization
	void Start () {
		aICharacter = GetComponent<AICharacterControl>();
		aICharacter.SetTarget(TargetCenter.transform);

		speechAnim = GetComponent<SpeechAnimScript>();
		eyesAnim = GetComponent<EyesAnimScript>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	private void selectTarget(int moveId){
		switch (moveId)
		{
    
			case 0: //Left target
				aICharacter.SetTarget(TargetLeft.transform);
				break;
			case 2: //Right target
				aICharacter.SetTarget(TargetRight.transform);
				break;
			default: //Center target
				aICharacter.SetTarget(TargetCenter.transform);
				break;
		}
	}

    
	private void selectExpression(int expressionId)
    {
		switch (expressionId)
        {
            case 1: //Smile
				eyesAnim.expression = EyesAnimScript.ExpressionFacialEnum.smile;
				speechAnim.expression = (speechAnim.expression == SpeechAnimScript.MounthExpressionEnum.speech) ? SpeechAnimScript.MounthExpressionEnum.speech : SpeechAnimScript.MounthExpressionEnum.smile;
                break;
            case 2: //Unhappy
				eyesAnim.expression = EyesAnimScript.ExpressionFacialEnum.unhappy;
				speechAnim.expression = (speechAnim.expression == SpeechAnimScript.MounthExpressionEnum.speech) ? SpeechAnimScript.MounthExpressionEnum.speech : SpeechAnimScript.MounthExpressionEnum.unhappy;
				break;
			case 3: //none
				eyesAnim.expression = EyesAnimScript.ExpressionFacialEnum.none;
				speechAnim.expression = (speechAnim.expression == SpeechAnimScript.MounthExpressionEnum.speech) ? SpeechAnimScript.MounthExpressionEnum.speech : SpeechAnimScript.MounthExpressionEnum.none;
				break;
            default:
				eyesAnim.expression = EyesAnimScript.ExpressionFacialEnum.idle;
				speechAnim.expression = (speechAnim.expression == SpeechAnimScript.MounthExpressionEnum.speech) ? SpeechAnimScript.MounthExpressionEnum.speech : SpeechAnimScript.MounthExpressionEnum.none;
				break;
        }
    }

	private void OnGUI()
    {
        GUILayout.BeginVertical(GUILayout.Width(Screen.width));     
        
		toolBarMoveId = GUILayout.Toolbar(toolBarMoveId, textToolBarMove);
		selectTarget(toolBarMoveId);

		GUILayout.BeginHorizontal();
		avatarSpeech = GUILayout.Toggle(avatarSpeech, "Parler");
		speechAnim.expression = avatarSpeech ? SpeechAnimScript.MounthExpressionEnum.speech : SpeechAnimScript.MounthExpressionEnum.none;

		if( GUILayout.Button("Bonjour"))
		{
			aICharacter.character.WaveTrigger();
		}

		if (GUILayout.Button("Prendre"))
        {
			aICharacter.character.PickupTrigger();
        }
      
		GUILayout.EndHorizontal();

		toolBarExpressionId = GUILayout.Toolbar(toolBarExpressionId, textToolBarExpression);
		selectExpression(toolBarExpressionId);

		GUILayout.EndVertical();

    }






}
