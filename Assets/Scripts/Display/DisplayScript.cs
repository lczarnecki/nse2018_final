﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisplayScript : MonoBehaviour {
	//public MovieTexture film;

	// Use this for initialization
	void Start () {

		Debug.Log("displays connected: " + Display.displays.Length);
        // Display.displays[0] is the primary, default display and is always ON.
        // Check if additional displays are available and activate each.
        int iDisplay = 1;
        foreach (var item in Display.displays)
        {
            if (iDisplay > 2)
                return;

            item.Activate();
            iDisplay++;
        }
     
	}

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.Escape))
        {
            Application.Quit();
        }
    } 
}
