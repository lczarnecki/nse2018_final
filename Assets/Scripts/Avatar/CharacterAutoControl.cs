﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterAutoControl : MonoBehaviour {

 
	private Animator animator;
	private AudioSource audioSource;

    public List<AudioClip> FootstepSounds;    
 
	private float turnAmount;
	private float forwardAmount;
	private float movingTurnSpeed = 360;
	private float stationaryTurnSpeed = 180;

	private float stepCycle = 0f;
	private float stepInterval = 0.3f;
	private float nextStep = 0f;

	private bool isGrounded;
	private List<Collider> collisions = new List<Collider>();

	void Start()
	{
		animator = GetComponent<Animator>();
		audioSource = GetComponent<AudioSource>();

	}

    private void OnCollisionEnter(Collision collision)
    {
        ContactPoint[] contactPoints = collision.contacts;
        for (int i = 0; i < contactPoints.Length; i++)
        {
            if (Vector3.Dot(contactPoints[i].normal, Vector3.up) > 0.5f)
            {
                if (!collisions.Contains(collision.collider))
                {
                    collisions.Add(collision.collider);
                }
                isGrounded = true;
            }
        }
    }

    private void OnCollisionStay(Collision collision)
    {
        ContactPoint[] contactPoints = collision.contacts;
        bool validSurfaceNormal = false;
        for (int i = 0; i < contactPoints.Length; i++)
        {
            if (Vector3.Dot(contactPoints[i].normal, Vector3.up) > 0.5f)
            {
                validSurfaceNormal = true; break;
            }
        }

        if (validSurfaceNormal)
        {
            isGrounded = true;
            if (!collisions.Contains(collision.collider))
            {
                collisions.Add(collision.collider);
            }
        }
        else
        {
            if (collisions.Contains(collision.collider))
            {
                collisions.Remove(collision.collider);
            }
            if (collisions.Count == 0) { isGrounded = false; }
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        if (collisions.Contains(collision.collider))
        {
            collisions.Remove(collision.collider);
        }
        if (collisions.Count == 0) { isGrounded = false; }
    }
   
	void ApplyExtraTurnRotation()
    {
        // help the character turn faster (this is in addition to root rotation in the animation)
        float turnSpeed = Mathf.Lerp(stationaryTurnSpeed, movingTurnSpeed, forwardAmount);
        transform.Rotate(0, turnAmount * turnSpeed * Time.deltaTime, 0);
    }

	public void Move(Vector3 move, Transform target)
    {
        

        if (move.magnitude > 1f) 
			move.Normalize();

		var magnitude = move.magnitude;
		if (move == Vector3.zero)
        {
    
			var angle = Vector3.SignedAngle(transform.forward, target.forward, Vector3.up);
			turnAmount = 0f;

			var sens = angle > 0 ? 1.0f : -1.0f;
            
			if( Mathf.Abs(angle) > 2f) 
			{
				turnAmount = 1f * sens;
				magnitude = 0.2f;
			}

        }
		else
		{
			move = transform.InverseTransformDirection(move);
            move = Vector3.ProjectOnPlane(move, Vector3.up);
            turnAmount = Mathf.Atan2(move.x, move.z);
			forwardAmount = move.z;

		}
        
        ApplyExtraTurnRotation();
      
        animator.SetBool("Grounded", isGrounded);
        animator.SetFloat("MoveSpeed", magnitude);

		ProgressStepCycle(magnitude);
    }

	public void WaveTrigger()
    {
        animator.SetTrigger("Wave");
    }

    public void PickupTrigger()
    {
        animator.SetTrigger("Pickup");
    }
    

	private void ProgressStepCycle(float speed)
    {
        if (speed > 0)
        {
            stepCycle += speed * Time.fixedDeltaTime;
        }

        if (!(stepCycle > nextStep))
        {
            return;
        }

        nextStep = stepCycle + stepInterval;

        PlayFootStepAudio();
    }

	private void PlayFootStepAudio()
    {
        if (!isGrounded)
        {
            return;
        }       

        int indexSound = Random.Range(0, FootstepSounds.Count);
		audioSource.clip = FootstepSounds[indexSound];
        audioSource.PlayOneShot(audioSource.clip);
    
    }
 
}
