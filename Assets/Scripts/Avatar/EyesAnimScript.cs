﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EyesAnimScript : MonoBehaviour {

	const int _LEFT_EYE_MATERIAL = 4; 
	const int _RIGHT_EYE_MATERIAL = 2; 

	public enum ExpressionFacialEnum { none, idle, smile, unhappy };
	public ExpressionFacialEnum expression;

	private int[] spriteBlinkRightEye = { 0, 3, 7, 4, 4, 7, 3, 0 };
	private int[] spriteBlinkLeftEye = { 3, 0, 4, 7, 7, 4, 0, 3 };
	private int indexBlink = 0;

    public int uvTileY = 4; // texture sheet columns 
    public int uvTileX = 2; // texture sheet rows
    public float RateSpeedExpression = 0.02f;

    private Vector2 size;
    private Vector2 offset;

    private float nextExpression = 0f;

	private Renderer rendererObject;
   
    
	void Start()
    {
        this.rendererObject = GetComponentInChildren<Renderer>();
        this.size = new Vector2(1.0f / uvTileY, 1.0f / uvTileX);
		IdleExpression();
    }


    private void IdleExpression()
	{
		if (this.nextExpression < Time.time)
        {
			drawImageByIndex(spriteBlinkRightEye[this.indexBlink], _RIGHT_EYE_MATERIAL);
			drawImageByIndex(spriteBlinkLeftEye[this.indexBlink], _LEFT_EYE_MATERIAL);

			if (this.indexBlink < spriteBlinkLeftEye.Length-1)
			{
				this.nextExpression = Time.time + this.RateSpeedExpression;
				this.indexBlink++;
			}
            else
			{
				this.nextExpression = Time.time + Random.Range(0.5f,5f);
				this.indexBlink = 0;
			}
        }
	}
   
	private void NoneExpression()
    {
		this.expression = ExpressionFacialEnum.none;
		this.indexBlink = 0;
        drawImageByIndex(0, _RIGHT_EYE_MATERIAL);
        drawImageByIndex(3, _LEFT_EYE_MATERIAL);
    }

	private void SmileExpression()
    {
        this.expression = ExpressionFacialEnum.smile;
		drawImageByIndex(4, _RIGHT_EYE_MATERIAL);
		drawImageByIndex(7, _LEFT_EYE_MATERIAL);
    }

	private void UnhappyExpression()
    {
        this.expression = ExpressionFacialEnum.unhappy;
		drawImageByIndex(1, _RIGHT_EYE_MATERIAL);
		drawImageByIndex(2, _LEFT_EYE_MATERIAL);
    }

	private void drawImageByIndex(int index, int materialIndex)
    {
        var uIndex = index % this.uvTileY;
        var vIndex = index / this.uvTileY;

        this.offset = new Vector2(uIndex * this.size.x, 1.0f - this.size.y - vIndex * this.size.y);
		rendererObject.materials[materialIndex].SetTextureOffset("_MainTex", this.offset);
		rendererObject.materials[materialIndex].SetTextureScale("_MainTex", this.size);
    }

    void Update()
    {
		switch (this.expression)
		{
			case ExpressionFacialEnum.none:
				NoneExpression();
				break;
			case ExpressionFacialEnum.smile:
				SmileExpression();
				break;
			case ExpressionFacialEnum.unhappy:
				UnhappyExpression();
				break;
			default:
				IdleExpression();
				break;
		}
    }

}
