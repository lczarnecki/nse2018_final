﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;


public enum ResponseVideoEnum { endurance, equipe, interieur, mer, montagne, performance, innovant, relax, sensation, stratege, ville };

public class ResponseVideo
{
    public string Label { get; set; }
    public string VideoName { get; set; }
}

public class LoadAndPlay : MonoBehaviour {

	public List<GameObject> Tvs;
    private Dictionary<ResponseVideoEnum, ResponseVideo> Videos;

    private void loadVideoList()
    {
        ResponseVideo item = new ResponseVideo();
        item.Label = "Endurance";
        item.VideoName = "effort-distance";
        Videos.Add(ResponseVideoEnum.endurance, item);

        item = new ResponseVideo();
        item.Label = "Equipe";
        item.VideoName = "equipe";
        Videos.Add(ResponseVideoEnum.equipe, item);

        item = new ResponseVideo();
        item.Label = "Intérieur";
        item.VideoName = "interieur";
        Videos.Add(ResponseVideoEnum.interieur, item);

        item = new ResponseVideo();
        item.Label = "Mer";
        item.VideoName = "mer";
        Videos.Add(ResponseVideoEnum.mer, item);

        item = new ResponseVideo();
        item.Label = "Montagne";
        item.VideoName = "montagne";
        Videos.Add(ResponseVideoEnum.montagne, item);

        item = new ResponseVideo();
        item.Label = "Performance";
        item.VideoName = "performance";
        Videos.Add(ResponseVideoEnum.performance, item);

        item = new ResponseVideo();
        item.Label = "Produit innovant";
        item.VideoName = "produit-innovant";
        Videos.Add(ResponseVideoEnum.innovant, item);

        item = new ResponseVideo();
        item.Label = "Prendre son temps" + "\n" + "instant présent";
        item.VideoName = "relax";
        Videos.Add(ResponseVideoEnum.relax, item);

        item = new ResponseVideo();
        item.Label = "Sensation forte";
        item.VideoName = "sensation-forte";
        Videos.Add(ResponseVideoEnum.sensation, item);

        item = new ResponseVideo();
        item.Label = "Stratege";
        item.VideoName = "stratege";
        Videos.Add(ResponseVideoEnum.stratege, item);

        item = new ResponseVideo();
        item.Label = "En ville";
        item.VideoName = "ville";
        Videos.Add(ResponseVideoEnum.ville, item);
    }

    void Start()
    {
        Videos = new Dictionary<ResponseVideoEnum, ResponseVideo>();
        loadVideoList();
    }

    public void PlayVideoOnTv(ResponseVideoEnum videoEnum, int tvId)
	{
        var item = Videos[videoEnum];
		StartCoroutine(CoroutineNoiseAndVideo(item.VideoName, item.Label, tvId));
	}

    private void PlayVideo(string videoName, int tvId)
	{

        var clip = Resources.Load<VideoClip>("Videos/Reponses/" + videoName);
        var vpTv = Tvs[tvId].GetComponentInChildren<VideoPlayer>();
		vpTv.clip = clip;
		vpTv.Play();
    }

	IEnumerator CoroutineNoiseAndVideo(string videoName, string labelVideo, int tvId)
	{
        var light = Tvs[tvId].GetComponentInChildren<Light>();
        var meshRender = GameObject.Find(Tvs[tvId].name + "/DalleVideo").GetComponent<Renderer>();
        meshRender.enabled = true;


        var txt = Tvs[tvId].GetComponentInChildren<TextMesh>();
        txt.text = "";
        light.intensity = 0f;



        PlayVideo("noise", tvId);
        light.intensity = 1.0f;


        yield return new WaitForSeconds(1.5f);
        txt.text = labelVideo;

        PlayVideo(videoName, tvId);

    }
    
}
