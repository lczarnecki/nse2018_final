﻿using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;

public class APIRequestModel
{
    public string Command { get; set; }
    public List<string> Parameters { get; set; }

    public string FirstParamter
    {
        get { return Parameters.FirstOrDefault().ToLower() ?? ""; }
    }
}

public class APIController : MonoBehaviour
{

    private UniWebServer.WebServer server;
    private GameManager gameManager;

    // Use this for initialization
    void Start ()
    {
        server = new UniWebServer.WebServer(5003, 2, true);
        server.HandleRequest += Server_HandleRequest;
        server.Start();

        gameManager = GameObject.Find("GameManager").GetComponentInChildren<GameManager>();
    }

    private void Server_HandleRequest(UniWebServer.Request request, UniWebServer.Response response)
    {
        response.statusCode = 200;
        APIRequestModel model = null;
        if (!string.IsNullOrEmpty(request.body))
        {
            model = JsonConvert.DeserializeObject<APIRequestModel>(request.body); 
        }
        switch (model.Command.ToLower())
        {
            case "speak":
                CommandSpeak(model);
                break;
            case "cameraforward":
                gameManager.CameraAnimation();
                break;
            case "restart":
                UnityEngine.SceneManagement.SceneManager.LoadScene(0);
                break;
            case "expression":
                CommandCharacterExpression(model);
                break;
            case "moveto":
                CommandCharacterMove(model);
                break;
            case "showtv":
                CommandShowTv(model);
                break;
            default:
                response.statusCode = 404;
                break;
        }
    }


    private void CommandSpeak(APIRequestModel model)
    {
        switch (model.FirstParamter)
        {
            case "start":
                gameManager.StartSpeak();
                break;
            case "stop":
                gameManager.StopSpeak();
                break;
            default:
                gameManager.StopSpeak();
                break;
        }
    }

    private void CommandCharacterExpression(APIRequestModel model)
    {
        float duration = 1.5f;
        EyesAnimScript.ExpressionFacialEnum expression = EyesAnimScript.ExpressionFacialEnum.idle;
        switch (model.FirstParamter)
        {
            case "smile":
                expression = EyesAnimScript.ExpressionFacialEnum.smile;
                break;
            case "none":
                expression = EyesAnimScript.ExpressionFacialEnum.none;
                break;
            case "unhappy":
                expression = EyesAnimScript.ExpressionFacialEnum.unhappy;
                break;
            default:
                expression = EyesAnimScript.ExpressionFacialEnum.idle;
                break;
        }
        
        if (model.Parameters.Count>1)
        {
            float.TryParse(model.Parameters[1], out duration);
        }
        gameManager.SelectExpression(expression, duration);
    }

    private void CommandCharacterMove(APIRequestModel model)
    {
        MoveCharacterEnum moveCharacterTo = MoveCharacterEnum.center;
        switch (model.FirstParamter)
        {
            case "left":
                moveCharacterTo = MoveCharacterEnum.left;
                break;
            case "right":
                moveCharacterTo = MoveCharacterEnum.right;
                break;
            case "onside":
                moveCharacterTo = MoveCharacterEnum.onSide;
                break;
            default:
                moveCharacterTo = MoveCharacterEnum.center;
                break;
        }

        gameManager.SelectTarget(moveCharacterTo);
    }

    private void CommandShowTv(APIRequestModel model)
    {
        ResponseVideoEnum videoEnum; ;
        string param1 = model.Parameters[0];
        string param2 = model.Parameters[1];
        int tvIndex = -1;

        if (model.Parameters.Count != 2)
        {
            Debug.Log("Erreur ShowTv: Paramétres manquants");
            return;
        }


        if (!System.Enum.IsDefined(typeof(ResponseVideoEnum), param1))
        {
            Debug.Log("Erreur ShowTv: le premier paramétre n'est pas correct");
            return;
        }
        System.Enum.TryParse<ResponseVideoEnum>(param1, out videoEnum);


        if (!int.TryParse(param2, out tvIndex) || tvIndex <0 || tvIndex > 6)
        {
            Debug.Log("Erreur ShowTv: le deuxiéme paramétre n'est pas correct");
            return;
        }

        gameManager.ShowVideoTV(videoEnum, tvIndex);
    }

}
