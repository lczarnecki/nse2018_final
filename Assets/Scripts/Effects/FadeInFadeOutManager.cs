﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum FadeCameraEnum {none =0, cameraFront=1, cameraLeft = 2, cameraRight=4 }

public class FadeInFadeOutManager : MonoBehaviour {

    private Image FadeImageCameraFront;
    private Image FadeImageCameraLeft;
    private Image FadeImageCameraRight;

    public Color ColorIn;
    public Color ColorOut;
    public float DurationTransition;

    private FadeCameraEnum isInTransition;
    private float transition;
    private bool isShowing;


	void Start ()
    {
        FadeImageCameraFront = GameObject.Find("CanvasFront/ImageFadeFront").GetComponentInChildren<Image>();
        FadeImageCameraLeft = GameObject.Find("CanvasLeft/ImageFadeLeft").GetComponentInChildren<Image>();
        FadeImageCameraRight = GameObject.Find("CanvasRight/ImageFadeRight").GetComponentInChildren<Image>();
    }
	
    public void Fade(bool showing, FadeCameraEnum camerasTransition)
    {
        isShowing = showing;
        isInTransition = camerasTransition;
        transition = (isShowing) ? 0 : 1;
    }

    void Update()
    {
        if(isInTransition == FadeCameraEnum.none)
        {
            return;
        }

        transition += (isShowing) ? Time.deltaTime * (1 / this.DurationTransition) : -Time.deltaTime * (1 / this.DurationTransition);


        if ((isInTransition & FadeCameraEnum.cameraFront) == FadeCameraEnum.cameraFront)
        {
            FadeImageCameraFront.color = Color.Lerp(ColorIn, ColorOut, transition);
        }
        if ((isInTransition & FadeCameraEnum.cameraLeft) == FadeCameraEnum.cameraLeft)
        {
            FadeImageCameraLeft.color = Color.Lerp(ColorIn, ColorOut, transition);
        }
        if ((isInTransition & FadeCameraEnum.cameraRight) == FadeCameraEnum.cameraRight)
        {
            FadeImageCameraRight.color = Color.Lerp(ColorIn, ColorOut, transition);
        }

        if(transition>1 || transition < 0)
        {
            isInTransition = FadeCameraEnum.none;
        }
    }
}
