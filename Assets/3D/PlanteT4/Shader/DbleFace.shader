Shader "DbleFace"
{
	Properties
	{
		_Smoothness("Smoothness", Range( 0 , 1)) = 0
		_AO("AO", 2D) = "bump" {}
		[HideInInspector] __dirty( "", Int ) = 1
		_Normal("Normal", 2D) = "bump" {}
		_Albedo("Albedo", 2D) = "white" {}
		_Specular("Specular", Range( 0 , 1)) = 0
	}

	SubShader
	{
		Tags{ "RenderType" = "Opaque"  "Queue" = "Geometry0" }
		Cull Off
		CGPROGRAM
		#include "UnityStandardUtils.cginc"
		#pragma target 3.0
		#pragma surface surf StandardSpecular keepalpha addshadow fullforwardshadows 
		struct Input
		{
			float2 uv_Normal;
			float2 uv_Albedo;
			float2 uv_AO;
		};

		uniform sampler2D _Normal;
		uniform sampler2D _Albedo;
		uniform float _Specular;
		uniform float _Smoothness;
		uniform sampler2D _AO;

		void surf( Input input , inout SurfaceOutputStandardSpecular output )
		{
			output.Normal = tex2D( _Normal,input.uv_Normal);
			output.Albedo = tex2D( _Albedo,input.uv_Albedo).xyz;
			float3 FLOATToFLOAT30=_Specular;
			output.Specular = FLOATToFLOAT30;
			output.Smoothness = _Smoothness;
			output.Occlusion = tex2D( _AO,input.uv_AO).x;
		}

		ENDCG
	}
	Fallback "Diffuse"
}
/*ASEBEGIN
Version=10
2299;175;1502;804;1218.3;376.6999;1.3;True;True
Node;AmplifyShaderEditor.SamplerNode;1;-462,-358;Property;_Albedo;Albedo;None;True;0;False;white;Auto;False;0,0;1.0
Node;AmplifyShaderEditor.SamplerNode;5;-465.2,-185.4;Property;_Normal;Normal;None;True;0;False;bump;Auto;True;0,0;1.0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;13,-201;True;StandardSpecular;DbleFace;False;False;False;False;False;False;False;False;False;False;False;False;Off;On;LEqual;Opaque;0.5;True;True;0;0,0,0;0,0,0;0,0,0;0,0,0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0,0,0
Node;AmplifyShaderEditor.RangedFloatNode;2;-420.2,190.6;Property;_Smoothness;Smoothness;0;0;1
Node;AmplifyShaderEditor.RangedFloatNode;7;-496.2001,50.79999;Property;_Specular;Specular;0;0;1
Node;AmplifyShaderEditor.SamplerNode;6;-488.8,303.7;Property;_AO;AO;None;True;0;False;bump;Auto;True;0,0;0.1
WireConnection;0;0;1;0
WireConnection;0;1;5;0
WireConnection;0;3;7;0
WireConnection;0;4;2;0
WireConnection;0;5;6;0
ASEEND*/
//CHKSM=D923A9D6A4F9DF581C3C13F61CE45D0992E927A3